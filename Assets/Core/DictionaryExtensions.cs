using System.Collections.Generic;

namespace Core
{
    public static class DictionaryExtensions
    {
        public static void AddOrReplace<Tk, Te>(this Dictionary<Tk, Te> dictionary, Tk key, Te element)
        {
            if (dictionary.ContainsKey(key))
            {
                dictionary[key] = element;
            }
            else
            {
                dictionary.Add(key, element);
            }
        }
    }
}