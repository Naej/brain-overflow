using TMPro;
using UnityEngine;

namespace BrainFuck.Output
{
    public class TMPCharOutput : MonoBehaviour, ICharOutput
    {
        [SerializeField] private TMP_Text _text;
        public void Print(char output)
        {
             _text.text += output;
        }

        public void Clear()
        {
            _text.text = "";
        }
    }
}