namespace BrainFuck.Output
{
    public interface ICharOutput
    {
        void Print(char output);
        void Clear();
    }
}