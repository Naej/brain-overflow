namespace BrainFuck.Input
{
    public interface ICharInput
    {
        bool HasNextInput();
        char NextInput();

        void Clear();
    }
}