using TMPro;
using UnityEngine;

namespace BrainFuck.Input
{
    public class TMPCharInput : MonoBehaviour, ICharInput
    {
        [SerializeField] private TMP_InputField _inputField;


        public bool HasNextInput()
        {
            return _inputField.text.Length > 0;
        }

        public char NextInput()
        {
            var value = _inputField.text[0];
            _inputField.text = _inputField.text[1..];
            return value;
        }

        public void Clear()
        {
            _inputField.text = "";
        }
    }
}