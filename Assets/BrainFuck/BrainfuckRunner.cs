using System.Collections.Generic;
using Core;

namespace BrainFuck
{
    using Input;
    using Output;
    using Storage;

    public class BrainfuckRunner
    {
        public static IEnumerator<int> CodeRunner(string code, ICharOutput output, ICharInput input, ICharStorage storage)
        {
            int indentLevel = 0;
            Dictionary<int, int> openBrackets = new();
            for (var i = 0; i < code.Length; i++)
            {
                switch (code[i])
                {
                    case '[':
                        indentLevel++;
                        openBrackets.AddOrReplace(indentLevel, i);
                        break;
                    case ']':
                        if (storage.Read() == 0)
                        {
                            indentLevel--;
                        }
                        else
                        {
                            i = openBrackets[indentLevel];
                        }

                        break;
                    case '>':
                        storage.NextCell();
                        break;
                    case '<':
                        storage.PreviousCell();
                        break;
                    case '.':
                        output.Print(storage.Read());
                        break;
                    case ',':
                        while (!input.HasNextInput())
                        {
                            yield return i;
                        }
                        storage.Store(input.NextInput());

                        break;
                    case '+':
                        storage.Store((char) (storage.Read() + 1));
                        break;
                    case '-':
                        storage.Store((char) (storage.Read() - 1));
                        break;
                }

                yield return i;
            }
        }
    }
}