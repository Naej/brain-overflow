namespace BrainFuck.Storage
{
    public interface ICharStorage
    {
        void NextCell();
        void PreviousCell();
        void Store(char value);
        char Read();

        void Clear();
    }
}