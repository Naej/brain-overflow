using System.Collections.Generic;
using Core;

namespace BrainFuck.Storage
{
    public class DictionaryCharStorage : ICharStorage
    {
        private int _pointer;
        private Dictionary<int, char> _storage = new ();
        
        void Store(int address, char value) => _storage.AddOrReplace(address, value);

        char Read(int address) => ReadOrEmpty(_storage, address);
        
        public void NextCell()
        {
            _pointer++;
        }

        public void PreviousCell()
        {
            _pointer--;
        }

        public void Store(char value)
        {
            Store(_pointer, value);
        }

        public char Read()
        {
            return Read(_pointer);
        }

        static char ReadOrEmpty(Dictionary<int, char> dictionary, int key)
        {
            if (dictionary.ContainsKey(key))
            {
                return dictionary[key];
            }
            return (char) 0;
        }

        public void Clear() => _storage.Clear();
    }
}