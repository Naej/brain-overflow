using System;
using System.Collections;
using BrainFuck;
using BrainFuck.Input;
using BrainFuck.Output;
using BrainFuck.Storage;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BrainOverflow
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] TMP_InputField _codeField;
        [SerializeField] TMPCharInput _machineInput;
        [SerializeField] Output _output;
        [SerializeField] private VisualStorage _storage;

        [SerializeField] private Button _runButton;
        [SerializeField] private Button _stopButton;

        [SerializeField] private int _stepsPerFrame = 5;
        [SerializeField] private bool _running;
        
        private IEnumerator _runner = null;
        
        public void OnEnable()
        {
            _runButton.onClick.AddListener(Run);
            _stopButton.onClick.AddListener(Stop);
        }

        public void Run()
        {
            _machineInput.Clear();
            _output.Clear();
            _storage.Clear();
            _runner = BrainfuckRunner.CodeRunner(_codeField.text, _output, _machineInput, _storage);
            _running = true;
        }

        public bool Step(int steps)
        {
            for (int i = 0; i < steps; i++)
            {
                if (!_runner.MoveNext())
                {
                    return false;
                }
            }

            return true;
        }

        private void Update()
        {
            if (_running)
            {
                _running = Step(_stepsPerFrame);
            }
        }

        public void Stop()
        {
            _running = false;
        }
    }
}