using Core;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace BrainOverflow
{
    public class CharDisplay : MonoBehaviour
    {
        [SerializeField] TMP_Text _charText;
        [SerializeField] TMP_Text _intText;
        [SerializeField] private Image _coloredImage;

        public char content
        {
            get => _charText.text[0];
            set => Display(value);
        }
        
        public void Display(char toDisplay)
        {
            _charText.text = toDisplay.ToString();
            var numValue = (ushort) toDisplay;
            _intText.text = numValue.ToString();
            _coloredImage.color = Color.HSVToRGB((numValue  * 1024f / ushort.MaxValue) % 1, 0.6f, 1);;
        }
    }
}