using System.Collections.Generic;
using BrainFuck.Storage;
using Core;
using UnityEngine;

namespace BrainOverflow
{
    public class VisualStorage : MonoBehaviour, ICharStorage
    {
        [SerializeField] private CharDisplay _cellPrefab;
        
        private Dictionary<int, CharDisplay> _storage = new ();
        private int _pointer;


        public void NextCell()
        {
            _pointer++;
            CreateMissingCells();
        }

        public void PreviousCell()
        {
            _pointer--;
            CreateMissingCells();
        }

        void CreateMissingCells()
        {
            if (_storage.ContainsKey(_pointer)) return;
            
            var newCell = Instantiate(_cellPrefab, transform);
            newCell.content = (char) 0;
            newCell.transform.SetSiblingIndex(_pointer);
            _storage.Add(_pointer,newCell);
        }

        public void Store(char value)
        {
            CreateMissingCells();
            _storage[_pointer].content = value;
        }

        public char Read()
        {
            CreateMissingCells();
            return _storage[_pointer].content;
        }

        public void Clear()
        {
            foreach (var cell in _storage.Values)
            {
                Destroy(cell.gameObject);
            }
            _storage.Clear();
            _pointer = 0;
            CreateMissingCells();
        }
    }
}