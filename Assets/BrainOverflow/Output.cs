using System.Collections.Generic;
using BrainFuck.Output;
using UnityEngine;

namespace BrainOverflow
{
    public class Output : MonoBehaviour, ICharOutput
    {
        [SerializeField] CharDisplay _elementPrefab;
        List<CharDisplay> _elements = new();
        
        public void Print(char output)
        {
            var element = Instantiate(_elementPrefab, transform);
            _elements.Add(element);
            element.content = output;
        }

        public void Clear()
        {
            foreach (var element in _elements)
            {
                Destroy(element.gameObject);
            }
            _elements.Clear();
        }
    }
}